package com.aeltus.generic.rest.client;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.aeltus.generic.rest.client")
public class AppConfig {
}
