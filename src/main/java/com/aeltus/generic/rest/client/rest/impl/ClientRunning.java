package com.aeltus.generic.rest.client.rest.impl;

import com.aeltus.generic.rest.client.beans.CurrentWeather;
import com.aeltus.generic.rest.client.rest.AbstractRestClient;
import com.aeltus.generic.rest.client.rest.RestClientConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ClientRunning {

    Logger LOGGER = LoggerFactory.getLogger(ClientRunning.class);

    @Autowired
    @Qualifier("weatherRestClient")
    OpenWeatherRestClient client;

    public void run(){
        LOGGER.debug("/================================================================================/");
        LOGGER.debug("/                                                                                /");
        LOGGER.debug("/                       EXEMPLE 1 : return a simple String                       /");
        LOGGER.debug("/                                                                                /");
        LOGGER.debug("/================================================================================/");

        runDayly(6618215);

        LOGGER.debug("/================================================================================/");
        LOGGER.debug("/                                                                                /");
        LOGGER.debug("/                      EXEMPLE 2 : return an Object instance                     /");
        LOGGER.debug("/                                                                                /");
        LOGGER.debug("/================================================================================/");

        runLocal(6618215);
    }


    /**
     * Exemple : Get the response as a simple String
     *
     * @param cityIdentifier
     */
    public void runDayly(int cityIdentifier){

        client.setLogginType(RestClientConst.REST_AUTH_TYPE_URL)
              .setEndPoint("http://api.openweathermap.org/data/2.5/forecast/daily?id="+Integer.toString(cityIdentifier))
              .setHttpMethod(HttpMethod.GET);

        ResponseEntity<String> response = client.getClientResponseAsString();

        LOGGER.debug("Status code : {}",response.getStatusCodeValue());
        LOGGER.debug("Status value : {}",response.getStatusCode().getReasonPhrase());

        LOGGER.debug("HEADER :");
        LOGGER.debug(response.getHeaders().toString());
        LOGGER.debug("BODY :");
        LOGGER.debug(response.getBody());
    }


    /**
     * Exemple : Get the response as an object
     *
     * @param cityIdentifier
     */
    public void runLocal(int cityIdentifier){

        client.setLogginType(RestClientConst.REST_AUTH_TYPE_URL)
              .setEndPoint("http://api.openweathermap.org/data/2.5/weather?id="+Integer.toString(cityIdentifier))
              .setHttpMethod(HttpMethod.GET);

        ResponseEntity<?> response = client.getClientResponseAsObject(CurrentWeather.class);

        LOGGER.debug("Status code : {}",response.getStatusCodeValue());
        LOGGER.debug("Status value : {}",response.getStatusCode().getReasonPhrase());

        LOGGER.debug("HEADER :");
        LOGGER.debug(response.getHeaders().toString());
        LOGGER.debug("BODY :");
        CurrentWeather currentWeather = (CurrentWeather) response.getBody();
        LOGGER.debug(currentWeather.toString());

    }

}
