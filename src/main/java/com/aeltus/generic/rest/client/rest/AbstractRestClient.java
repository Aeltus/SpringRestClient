package com.aeltus.generic.rest.client.rest;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
@Scope("prototype")
public abstract class AbstractRestClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRestClient.class);

    private String endPoint;
    private String token;
    private String errorHandlerLevel = RestClientConst.REST_ERROR_HANDLER_LOG;
    private String logginType = RestClientConst.REST_AUTH_TYPE_NONE;
    private String logginPrefix;
    private HttpMethod httpMethod = HttpMethod.GET;
    private Map<String, String> headers = new HashMap<>();
    private String contentType = MediaType.APPLICATION_JSON_UTF8_VALUE;
    private int readTimeout = RestClientConst.REST_CLIENT_DEFAULT_TIMEOUT;
    private int connectionTimeout = RestClientConst.REST_CLIENT_DEFAULT_TIMEOUT;

    /**
     * return the response of the request as a String value
     *
     * @return ResponseEntity<String>
     */
    public ResponseEntity<String> getClientResponseAsString() {
        RestTemplate restTemplate = prepareRestTemplate();

        return restTemplate.exchange(getResourceURL(), getHttpMethod(), new HttpEntity<String>(createHeaders()), String.class);
    }

    /**
     * Return the response of the request, set the body of the request
     * from the param obj
     *
     * @param obj
     * @return ResponseEntity<String>
     */
    public ResponseEntity<String> getClientResponseAsString(Object obj) {
        RestTemplate restTemplate = prepareRestTemplate();

        return restTemplate.exchange(getResourceURL(), getHttpMethod(), new HttpEntity<>(obj, createHeaders()), String.class);
    }

    /**
     * return the response of the request as an objectInstance
     *
     * @return ResponseEntity<?>
     */
    public ResponseEntity<?> getClientResponseAsObject(Class<?> clazz) {
        RestTemplate restTemplate = prepareRestTemplate();

        return restTemplate.exchange(getResourceURL(), getHttpMethod(), new HttpEntity<>(createHeaders()), clazz);
    }

    /**
     * return the response of the request as an objectInstance and set the body of the request
     *
     * @return ResponseEntity<?>
     */
    public ResponseEntity<?> getClientResponseAsObject(Class<?> clazz, Object obj) {
        RestTemplate restTemplate = prepareRestTemplate();

        return restTemplate.exchange(getResourceURL(), getHttpMethod(), new HttpEntity<>(obj, createHeaders()), clazz);
    }

    /**
     * prepare the rest template and headers for the request
     *
     * @return RestTemplate
     */
    public RestTemplate prepareRestTemplate(){
        RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());

        restTemplate.setErrorHandler(new ErrorHandler(errorHandlerLevel));

        LOGGER.debug("Trying to get the endpoint :");
        LOGGER.debug(getResourceURL());
        LOGGER.debug("With HTTP Method : {}",httpMethod);
        if (logginType.equals(RestClientConst.REST_AUTH_TYPE_HEADER)){
            LOGGER.debug("Header authentication header setted to : {} : {}",logginPrefix, token);
            this.headers.put(logginPrefix, token);
        }
        this.headers.put(HttpHeaders.CONTENT_TYPE, contentType);
        LOGGER.debug("Content type setted to : {}",contentType);

        return restTemplate;
    }

    /**
     * Needed to customize read timeout and connection timeout
     *
     * @return clientHttpRequestFactory
     */
    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(connectionTimeout);
        clientHttpRequestFactory.setReadTimeout(readTimeout);
        return clientHttpRequestFactory;
    }

    /**
     * return the customized resource url
     *
     * @return String
     */
    public String getResourceURL(){
        String resourceUrl;
        String separator;
        if (logginType.equals(RestClientConst.REST_AUTH_TYPE_URL)){
            if (Strings.isNullOrEmpty(logginPrefix)){
                LOGGER.warn("Any login prefix defined");
            }
            if (endPoint.contains("?")){
                separator = "&";
            } else {
                separator = "?";
            }
            resourceUrl = endPoint+separator+logginPrefix+"="+token;
        } else {
            resourceUrl = endPoint;
        }
        return resourceUrl;
    }

    /**
     * Create the headers of the request
     * @return headers
     */
    public HttpHeaders createHeaders(){
        HttpHeaders headers = new HttpHeaders();
        if (!this.headers.isEmpty()){
            for(Map.Entry<String, String> e : this.headers.entrySet()){
                LOGGER.debug("Header added : {} : {}",e.getKey(), e.getValue());
                headers.set(e.getKey(), e.getValue());
            }
        }
        return  headers;
    }

    /**
     * Add a header for the request
     *
     * @param key
     * @param value
     * @return this
     */
    public AbstractRestClient addHeader(String key, String value){
        headers.put(key, value);
        return this;
    }

    /**
     * Remove a header in the list
     *
     * @param key
     * @return this
     */
    public AbstractRestClient removeHeader(String key){
        headers.remove(key);
        return this;
    }

    /**
     * Replace a header in the list
     *
     * @param key
     * @param value
     * @return this
     */
    public AbstractRestClient replaceHeader(String key, String value){
        headers.replace(key, value);
        return this;
    }

    /**
     * Set the method for the request
     *     => HttpMethod.GET by default
     * @param method
     * @return this
     */
    public final AbstractRestClient setHttpMethod(HttpMethod method){
        this.httpMethod = method;
        return this;
    }

    /**
     * set the endpoint for the request
     *
     * @param endPoint
     * @return this
     */
    public final AbstractRestClient setEndPoint(String endPoint) {
        this.endPoint = endPoint;
        return this;
    }

    /**
     * Set the authentification token
     *
     * @param token
     * @return this
     */
    public final AbstractRestClient setToken(String token) {
        this.token = token;
        return this;
    }

    /**
     * Set the type of login (NONE, HEADER or URL)
     *     => RestClientConst.REST_AUTH_TYPE_NONE by default
     * @param logginType
     * @return this
     */
    public final AbstractRestClient setLogginType(String logginType) {
        this.logginType = logginType;
        return this;
    }

    /**
     * Set the prefix for login
     *
     * @param logginPrefix
     * @return this
     */
    public final AbstractRestClient setLogginPrefix(String logginPrefix) {
        this.logginPrefix = logginPrefix;
        return this;
    }

    /**
     * Set the attempted content type
     *     => MediaType.APPLICATION_JSON_UTF8_VALUE by default
     * @param contentType
     * @return this
     */
    public AbstractRestClient setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    /**
     * Set the timeout of the response
     *  => RestClientConst.REST_CLIENT_DEFAULT_TIMEOUT by default
     *
     * @param timeout
     * @return this
     */
    public AbstractRestClient setTimeout(int timeout) {
        this.readTimeout = timeout;
        return this;
    }

    public final String getToken(){
        return this.token;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public String getErrorHandlerLevel() {
        return errorHandlerLevel;
    }

    public final HttpMethod getHttpMethod(){
        return this.httpMethod;
    }

    public AbstractRestClient setErrorHandlerLevel(String errorHandlerLevel) {
        this.errorHandlerLevel = errorHandlerLevel;
        return this;
    }

    public String getLogginType() {
        return logginType;
    }

    public String getLogginPrefix() {
        return logginPrefix;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public AbstractRestClient setHeaders(Map<String, String> headers) {
        this.headers = headers;
        return this;
    }

    public String getContentType() {
        return contentType;
    }

    public int getTimeout() {
        return readTimeout;
    }

    /**
     * Must contain the necessary to configure the specific client
     */
    public abstract void init();

    /**
     * Must contain the method to get de authentification token, and return it as a string
     * @return token
     */
    public abstract String getAuthToken();

}
