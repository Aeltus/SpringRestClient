package com.aeltus.generic.rest.client.Exceptions;

import java.io.IOException;

public class BackendException extends IOException {

    public BackendException(String message){
        super(message);
    }

    public BackendException(Throwable t){
        super(t);
    }
    public BackendException(String message, Throwable t){
        super(message, t);
    }


}
