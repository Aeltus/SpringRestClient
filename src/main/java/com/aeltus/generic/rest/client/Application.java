package com.aeltus.generic.rest.client;

import com.aeltus.generic.rest.client.rest.impl.ClientRunning;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String args[]) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);

        ClientRunning runner = (ClientRunning) ctx.getBean("clientRunning");
        runner.run();
    }
}
