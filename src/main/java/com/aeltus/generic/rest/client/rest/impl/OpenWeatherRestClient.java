package com.aeltus.generic.rest.client.rest.impl;

import com.aeltus.generic.rest.client.rest.AbstractRestClient;
import com.aeltus.generic.rest.client.rest.RestClientConst;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service("weatherRestClient")
@Scope("prototype")
public class OpenWeatherRestClient extends AbstractRestClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRestClient.class);

    public OpenWeatherRestClient(){
        //empty constructor
    }

    @PostConstruct
    public void init(){
        LOGGER.debug("Initializing {} rest client", OpenWeatherRestClient.class);
        this.setLogginPrefix("APPID");
        this.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        if (Strings.isNullOrEmpty(this.getToken())){
            LOGGER.debug("AuthToken is null, getting token...");
            this.setToken(getAuthToken());
            LOGGER.debug("Token recovered...");
        }
    }

    public String getAuthToken(){
        this.setLogginType(RestClientConst.REST_AUTH_TYPE_NONE);
        return "2c19b5539b9a40d2d77e46e3db8bed0a";
    }

}
