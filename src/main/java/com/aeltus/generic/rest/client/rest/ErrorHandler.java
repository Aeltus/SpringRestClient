package com.aeltus.generic.rest.client.rest;

import com.aeltus.generic.rest.client.Exceptions.BackendException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.IOException;

public class ErrorHandler extends DefaultResponseErrorHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);
    private String errorLevel = RestClientConst.REST_ERROR_HANDLER_ERROR;

    public ErrorHandler(String level){
        this.errorLevel = level;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {

        if (errorLevel.equals(RestClientConst.REST_ERROR_HANDLER_ERROR)){
            LOGGER.error("An error occured while getting api response : error {} => {}",response.getStatusCode().value(),response.getStatusCode().getReasonPhrase());
            throw new BackendException("An error occured while getting api response : error "
                                                                +response.getStatusCode().value()
                                                                +" => "+response.getStatusCode().getReasonPhrase());
        } else if (errorLevel.equals(RestClientConst.REST_ERROR_HANDLER_LOG)){
            LOGGER.error("An error occured while getting api response : error {} => {}",response.getStatusCode().value(),response.getStatusCode().getReasonPhrase());
        }
    }
}