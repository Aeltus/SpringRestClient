package com.aeltus.generic.rest.client.rest;

public class RestClientConst {

    public static final String REST_AUTH_TYPE_URL = "URL";
    public static final String REST_AUTH_TYPE_HEADER = "HEADER";
    public static final String REST_AUTH_TYPE_NONE = "NONE";

    public static final String REST_ERROR_HANDLER_NONE = "NONE";
    public static final String REST_ERROR_HANDLER_LOG = "LOG";
    public static final String REST_ERROR_HANDLER_ERROR = "ERROR";

    public static final int REST_CLIENT_DEFAULT_TIMEOUT = 15000;

    private RestClientConst(){
        //empty constructor
    }
}
